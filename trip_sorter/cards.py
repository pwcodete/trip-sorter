from dataclasses import dataclass


__all__ = ('Card', 'TransportCard', 'AirportBusCard', 'TrainCard', 'PlaneCard', 'FinalDestinationCard')


@dataclass
class Card:
    pass


@dataclass
class TransportCard(Card):
    source: str
    destination: str

    def __str__(self):
        return f'From {self.source} to {self.destination}.'


@dataclass
class NumberSeatMixin:
    number: str
    seat: str


@dataclass
class AirportBusCard(TransportCard):
    def __str__(self):
        return f'Take the airport bus from {self.source} to {self.destination} Airport. No seat assignment.'


@dataclass
class TrainCard(NumberSeatMixin, TransportCard):
    def __str__(self):
        return f'Take train {self.number} from {self.source} to {self.destination}. Sit in seat {self.seat}.'


@dataclass
class PlaneCard(NumberSeatMixin, TransportCard):
    gate: str
    baggage_info: str

    def __str__(self):
        return f'From {self.source}, take flight {self.number} to {self.destination}. ' \
               f'Gate {self.gate}, seat {self.seat}. {self.baggage_info}'


@dataclass
class FinalDestinationCard(Card):
    def __str__(self):
        return 'You have arrived at your final destination.'
