from typing import List, Set

from .cards import Card, FinalDestinationCard, TransportCard
from .exceptions import CircularTripError, IncompleteTripError, TripBifurcationError


__all__ = ('sort_cards',)


def _get_first_element_from_diff(first: Set[str], second: Set[str]) -> str:
    """ Get single element from diff between first and second set.
        Function used to detect source and target of a trip.
    """

    result = first - second
    diff_size = len(result)

    if diff_size > 1:
        raise IncompleteTripError
    elif diff_size == 0:
        raise CircularTripError

    return result.pop()


def sort_cards(cards: List[TransportCard]) -> List[Card]:
    """ Sort list of cards. """

    # creates sets of sources, destinations and mapping source -> card
    sources = {card.source for card in cards}
    if len(sources) != len(cards):
        raise TripBifurcationError

    destinations = {card.destination for card in cards}
    cards_map = {card.source: card for card in cards}

    # detect starting location and destination of the trip
    global_source = _get_first_element_from_diff(sources, destinations)
    global_destination = _get_first_element_from_diff(destinations, sources)

    # go from starting location to destination step-by-step
    current = cards_map[global_source]
    result = [current]
    while current.destination != global_destination:
        current = cards_map[current.destination]
        result.append(current)

    return result + [FinalDestinationCard()]
