__all__ = ('TripSorterError', 'IncompleteTripError', 'CircularTripError', 'TripBifurcationError')


class TripSorterError(Exception):
    """ Base exception for trip sorting algorithm. """


class IncompleteTripError(TripSorterError):
    """ Exception indicating incomplete trip. """


class CircularTripError(TripSorterError):
    """ Exception indicating circular connections with given cards. """


class TripBifurcationError(TripSorterError):
    """ Exception indicating bifurcation in trip. """
