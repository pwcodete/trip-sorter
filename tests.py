from unittest import main, TestCase

from trip_sorter import sort_cards, \
    CircularTripError, IncompleteTripError, TripBifurcationError, \
    AirportBusCard, TrainCard, PlaneCard, FinalDestinationCard


class TestCards(TestCase):
    def test_airport_bus_card_to_string(self):
        card = AirportBusCard(source='Barcelona', destination='Gerona Airport')
        expected_message = 'Take the airport bus from Barcelona to Gerona Airport Airport. No seat assignment.'

        self.assertEqual(str(card), expected_message)

    def test_train_card_to_string(self):
        card = TrainCard(source='Madrid', destination='Barcelona', number='78A', seat='45B')
        expected_message = 'Take train 78A from Madrid to Barcelona. Sit in seat 45B.'

        self.assertEqual(str(card), expected_message)

    def test_plane_card_to_string(self):
        card = PlaneCard(source='Gerona Airport', destination='Stockholm', number='SK455', seat='3A', gate='45B',
                         baggage_info='Baggage drop at ticket counter 344.')
        expected_message = 'From Gerona Airport, take flight SK455 to Stockholm. Gate 45B, seat 3A. ' \
                           'Baggage drop at ticket counter 344.'

        self.assertEqual(str(card), expected_message)

    def test_final_destination_card_to_string(self):
        card = FinalDestinationCard()
        expected_message = 'You have arrived at your final destination.'

        self.assertEqual(str(card), expected_message)


class TestTripSorter(TestCase):
    def test_proper_trip(self):
        cards = [
            AirportBusCard(source='Barcelona', destination='Gerona Airport'),
            PlaneCard(source='Stockholm', destination='New York JFK', number='SK22', seat='7B', gate='22',
                      baggage_info='Baggage will we automatically transferred from your last leg.'),
            PlaneCard(source='Gerona Airport', destination='Stockholm', number='SK455', seat='3A', gate='45B',
                      baggage_info='Baggage drop at ticket counter 344.'),
            TrainCard(source='Madrid', destination='Barcelona', number='78A', seat='45B'),
        ]
        expected_result = [
            TrainCard(source='Madrid', destination='Barcelona', number='78A', seat='45B'),
            AirportBusCard(source='Barcelona', destination='Gerona Airport'),
            PlaneCard(source='Gerona Airport', destination='Stockholm', number='SK455', seat='3A', gate='45B',
                      baggage_info='Baggage drop at ticket counter 344.'),
            PlaneCard(source='Stockholm', destination='New York JFK', number='SK22', seat='7B', gate='22',
                      baggage_info='Baggage will we automatically transferred from your last leg.'),
            FinalDestinationCard()
        ]

        result = sort_cards(cards)
        self.assertEqual(result, expected_result)

    def test_incomplete_trip(self):
        cards = [
            AirportBusCard(source='Barcelona', destination='Gerona Airport'),
            PlaneCard(source='Stockholm', destination='New York JFK', number='SK22', seat='7B', gate='22',
                      baggage_info='Baggage will we automatically transferred from your last leg.'),
            TrainCard(source='Madrid', destination='Barcelona', number='78A', seat='45B'),
        ]

        with self.assertRaises(IncompleteTripError):
            sort_cards(cards)

    def test_circular_trip(self):
        cards = [
            AirportBusCard(source='Barcelona', destination='Gerona Airport'),
            PlaneCard(source='Stockholm', destination='Madrid', number='SK22', seat='7B', gate='22',
                      baggage_info='Baggage will we automatically transferred from your last leg.'),
            PlaneCard(source='Gerona Airport', destination='Stockholm', number='SK455', seat='3A', gate='45B',
                      baggage_info='Baggage drop at ticket counter 344.'),
            TrainCard(source='Madrid', destination='Barcelona', number='78A', seat='45B'),
        ]

        with self.assertRaises(CircularTripError):
            sort_cards(cards)

    def test_trip_with_bifurcation(self):
        cards = [
            AirportBusCard(source='Barcelona', destination='Gerona Airport'),
            TrainCard(source='Barcelona', destination='Gerona Airport', number='79B', seat='34A'),
            PlaneCard(source='Stockholm', destination='New York JFK', number='SK22', seat='7B', gate='22',
                      baggage_info='Baggage will we automatically transferred from your last leg.'),
            PlaneCard(source='Gerona Airport', destination='Stockholm', number='SK455', seat='3A', gate='45B',
                      baggage_info='Baggage drop at ticket counter 344.'),
            TrainCard(source='Madrid', destination='Barcelona', number='78A', seat='45B'),
        ]

        with self.assertRaises(TripBifurcationError):
            sort_cards(cards)


if __name__ == '__main__':
    main().runTests()
