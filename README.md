# trip-sorter by Piotr Wawryka

# Requirements
Python 3.7 is required. The easiest way is to setup environment using pyenv tool.
Details: https://github.com/pyenv/pyenv

# Run tests
Run `python tests.py` in main directory.

For API usage examples please take a look at tests.py

# Time complexity
According to: https://wiki.python.org/moin/TimeComplexity 

Average case: O(n)

Worst case: O(n^2)
